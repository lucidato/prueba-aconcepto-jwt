package com.autoself.securityjwt.util;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.web.ResourceProperties.Chain;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import com.autoself.securityjwt.dto.User;
import com.autoself.securityjwt.service.MyUserDetailService;

@Component
public class Filter extends OncePerRequestFilter{

	@Autowired
	private JwtUtil jwtUtil;
	@Autowired
	private MyUserDetailService detailService;
	
	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		
		final String autorizaionHeader = request.getHeader("Authorization");
		User user= new User();
		
		// ojo
		if (autorizaionHeader != null && autorizaionHeader.startsWith("Bearer ")) {
			user.setToken(autorizaionHeader.substring(7));
			user.setUsername(jwtUtil.extractUserName(user.getToken()));
		}
		
		if (user.getUsername()!= null && SecurityContextHolder.getContext().getAuthentication() == null) {
			
			UserDetails userDetail = this.detailService.loadUserByUsername(user.getUsername());
			
			if (jwtUtil.validateToken(user.getToken(), userDetail)) {
				
				UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(userDetail,null, userDetail.getAuthorities());
				usernamePasswordAuthenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
				SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
			}
		}
		
		filterChain.doFilter(request, response);
		
		
	}

	
}
