package com.autoself.securityjwt.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/general")
public class HelloController {
	
	@GetMapping("/hola")
	public ResponseEntity<String> hello(){
		return ResponseEntity.status(HttpStatus.OK).body("hola");
	}

}
