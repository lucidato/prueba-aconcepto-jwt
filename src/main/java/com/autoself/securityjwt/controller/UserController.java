package com.autoself.securityjwt.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.autoself.securityjwt.dto.User;
import com.autoself.securityjwt.service.MyUserDetailService;
import com.autoself.securityjwt.util.JwtUtil;

@RestController
public class UserController {

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private MyUserDetailService userDetailsService;

	@Autowired
	private JwtUtil jwtUti;

	@PostMapping("/login")
	public ResponseEntity<User> login(@RequestBody User user) throws Exception {

		try {
			authenticationManager
					.authenticate(new UsernamePasswordAuthenticationToken(user.getUsername(), user.getPassword()));

		} catch (BadCredentialsException e) {
			throw new BadCredentialsException(" error crenciales");
		}

		final UserDetails userDetail = userDetailsService.loadUserByUsername(user.getUsername());

		user.setToken(jwtUti.generateToken(userDetail));
		return ResponseEntity.status(HttpStatus.OK).body(user);

	}

}
